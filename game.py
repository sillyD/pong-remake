import pygame
from ball import Ball
from platforms import Platform
pygame.font.init()

# representing our game
class Game:

    def __init__(self, screen):
        # starting game
        self.is_playing = False
        self.at_start = False
        self.verify_showing_controls = False
        self.round = 0

        # define the number of collisions
        self.number_collisions = 0

        # define the ball
        self.all_balls = pygame.sprite.Group()
        self.ball = Ball(screen, self)
        self.all_balls.add(self.ball)

        # define the platforms
        self.all_platforms = pygame.sprite.Group()
        self.platform1 = Platform(-15, 0)
        self.platform2 = Platform(screen.get_width() - 65, 0)
        self.all_platforms.add(self.platform1, self.platform2)

        # define the points
        self.player1_initials_points = 0
        self.player1_points = 0
        self.player2_initials_points = 0
        self.player2_points = 0

        # define the score surface
        self.score_surface = None

        # managing the pressed buttons
        self.pressed = {}

    def show_controls(self, screen):
        # controls informations
        my_font = pygame.font.SysFont('Arial', 20)
        player1_text_surface = my_font.render("PLAYER 1", True, (255, 255, 255), (0, 0, 0))
        controls1_text_surface = my_font.render("UP and DOWN arrow keys !", True, (255, 255, 255), (0, 0, 0))
        player2_text_surface = my_font.render("PLAYER 2", True, (255, 255, 255), (0, 0, 0))
        controls2_text_surface = my_font.render("W and S arrow keys !", True, (255, 255, 255), (0, 0, 0))
        # button used to return to the main menu
        back_button = pygame.image.load('ressources/back.png')
        back_button = pygame.transform.scale(back_button, (90, 90))
        back_button_rect = back_button.get_rect()
        back_button_rect.x = 470
        back_button_rect.y = 300
        # we blit all the components
        screen.blit(player1_text_surface, (50, 60))
        screen.blit(controls1_text_surface, (50, 80))
        screen.blit(player2_text_surface, (420, 60))
        screen.blit(controls2_text_surface, (420, 80))
        screen.blit(back_button, back_button_rect)
        # verifying if we click the back_button
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if back_button_rect.collidepoint(event.pos):
                    self.verify_showing_controls = False

    def start(self, screen):
        self.is_playing = True
        self.at_start = True

    def ball_over(self, screen):
        self.at_start = True
        self.ball = Ball(screen, self)
        self.platform1.rect.y = 0
        self.platform2.rect.y = 0

    def game_over(self, player, screen):
        # defining the font
        my_font = pygame.font.SysFont('Arial', 35)
        # refresh the score
        self.score_surface = my_font.render('player2  {} - {}  player1'.format(self.player2_points, self.player1_points), False, (255, 255, 255), (133, 108, 202))
        screen.blit(self.score_surface, (screen.get_width() / 2 - 150, 0))
        # show the winner
        text_surface = my_font.render('{} win'.format(player), False, (255, 255, 255), (133, 108, 202))
        screen.blit(text_surface, (screen.get_width()/2, screen.get_height()/2))
        pygame.display.flip()
        pygame.time.wait(2000)
        text_surface = my_font.render('Going back to the menu...', False, (255, 255, 255), (133, 108, 202))
        screen.blit(text_surface, (screen.get_width()/2-50, screen.get_height()-120))
        pygame.display.flip()
        pygame.time.wait(3000)
        self.is_playing = False
        self.round = 0
        # we re-init all the components
        self.player1_points = self.player1_initials_points
        self.player2_points = self.player2_initials_points
        self.ball = Ball(screen, self)
        self.platform1.rect.y = 0
        self.platform2.rect.y = 0

    def high_speed(self):

        # on verifie s'il y'a eu au moins 3 collisions
        if self.number_collisions == 3:
            # on augmente la vitesse et on remet le nbre de collisions a 0
            self.ball.velocity += 1
            self.number_collisions = 0

    def counter(self, screen):
        self.round += 1
        i = 3
        while i != 0:
            my_font = pygame.font.SysFont('Arial', 50)
            round_surface = my_font.render('ROUND {}'.format(self.round), True, (255, 255, 255), (133, 108, 202))
            text_surface = my_font.render('{}'.format(i), True, (255, 255, 255), (133, 108, 202))
            screen.blit(round_surface, (screen.get_width()/2-90, screen.get_height()/2-90))
            screen.blit(text_surface, (screen.get_width()/2-7, screen.get_height()/2-15))
            pygame.display.flip()
            pygame.time.wait(1000)
            i -= 1
        self.at_start = False

    def update(self, screen):

        # put the ball's image
        screen.blit(self.ball.image, self.ball.rect)

        # put the platforms
        screen.blit(self.platform1.image, self.platform1.rect)
        screen.blit(self.platform2.image, self.platform2.rect)

        # put the score
        my_font = pygame.font.SysFont('Arial', 35)
        self.score_surface = my_font.render('player2  {} - {}  player1'.format(self.player2_points, self.player1_points), False, (255, 255, 255), (133, 108, 202))
        screen.blit(self.score_surface, (screen.get_width()/2-150, 0))

        # move the ball an manage the speed
        self.ball.move(screen)
        self.high_speed()

        # we check the platform1's moves
        if self.pressed.get(pygame.K_UP) and self.platform2.rect.y > 0:
            self.platform2.move_up()
        elif self.pressed.get(pygame.K_DOWN) and self.platform2.rect.y < screen.get_height()-80:
            self.platform2.move_down()

        # we check the platform2's moves
        if self.pressed.get(pygame.K_w) and self.platform1.rect.y > 0:
            self.platform1.move_up()
        elif self.pressed.get(pygame.K_s) and self.platform1.rect.y < screen.get_height()-80:
            self.platform1.move_down()

    def check_collision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)

