import pygame

class Platform(pygame.sprite.Sprite):

    def __init__(self, x, y):
        super().__init__()
        self.velocity = 2
        self.image = pygame.image.load('ressources/plateforme.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def move_up(self):
        self.rect.y -= self.velocity

    def move_down(self):
        self.rect.y += self.velocity

