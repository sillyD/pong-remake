import pygame
from game import Game

pygame.mixer.pre_init(44100, -16, 2, 2048) # setup mixer to avoid sound lag

pygame.init()

#pygame.mixer.init()

# generate the window's game
pygame.display.set_caption("Fighter Ball")
screen = pygame.display.set_mode((630, 400))

# load the background
backg = pygame.image.load('ressources/background.png')

# banner loading
banner = pygame.image.load('ressources/smash.png')
banner = pygame.transform.scale(banner, (200, 200))
banner_rect = banner.get_rect()
banner_rect.x = 230

# play button
play_button = pygame.image.load('ressources/play.png')
play_button = pygame.transform.scale(play_button, (120, 120))
play_button_rect = play_button.get_rect()
play_button_rect.x = 130
play_button_rect.y = 200

# show_controls button
show_control_button = pygame.image.load('ressources/controls.png')
show_control_button = pygame.transform.scale(show_control_button, (120, 120))
show_control_button_rect = show_control_button.get_rect()
show_control_button_rect.x = 360
show_control_button_rect.y = 200
my_font = pygame.font.SysFont('Arial', 20)
text_surface = my_font.render("CONTROLS", True, (255, 255, 255), (0, 0, 0))

# load the game
game = Game(screen)

# load the song
song = pygame.mixer.music.load("ressources/sounds/son.ogg")
pygame.mixer.music.play(-1)

running = True

# game loop
while running:

    # put the background
    screen.blit(backg, (0, 0))

    # updating the game if it's running
    if game.is_playing:
        if game.at_start:
            game.counter(screen)
        else:
            game.update(screen)
    else:
        # showing controls or not
        if game.verify_showing_controls:
            game.show_controls(screen)
        else:
            # adding the banner and the button
            screen.blit(play_button, play_button_rect)
            screen.blit(banner, banner_rect)
            screen.blit(show_control_button, show_control_button_rect)
            screen.blit(text_surface, (375, 330))

    # screen updating
    pygame.display.flip()

    for event in pygame.event.get():
        # closing window
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True
        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if play_button_rect.collidepoint(event.pos):
                game.start(screen)
            elif show_control_button_rect.collidepoint(event.pos):
                game.verify_showing_controls = True

