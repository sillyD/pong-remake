import pygame

class Ball(pygame.sprite.Sprite):

    def __init__(self, screen, game):
        super().__init__()
        self.game = game
        self.velocity = 2
        self.image = pygame.image.load('ressources/ball2.png')
        self.rect = self.image.get_rect()
        self.rect.x = screen.get_width()/2
        self.rect.y = screen.get_height()/2
        self.dir_x = -self.velocity
        self.dir_y = -self.velocity

    def add_velocity(self):
        self.velocity += (self.velocity + 1) / 100

    def move(self, screen):
        self.rect.x += self.dir_x
        self.rect.y += self.dir_y

        if self.game.check_collision(self, self.game.all_platforms):
            self.dir_x = -self.dir_x
            self.game.number_collisions += 1
            print(self.game.number_collisions)
        elif self.rect.x < 0:
            self.game.player1_points += 1
            if self.game.player1_points == 3:
                self.game.game_over('player 1', screen)
            else:
                self.game.ball_over(screen)
        elif self.rect.x > screen.get_width():
            self.game.player2_points += 1
            if self.game.player2_points == 3:
                self.game.game_over('player 2', screen)
            else:
                self.game.ball_over(screen)
        elif self.rect.y < 0 or self.rect.y > screen.get_height()-25:
            self.dir_y = -self.dir_y




